FROM node:18.12

COPY server.js /app/server.js

CMD ["node", "/app/server.js"]
 